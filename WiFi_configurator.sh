#!/bin/bash

apartm=`cat /home/pi/apartment.txt`
base_apartment="base_"$apartm
INPUT=Passwords.csv
OLDIFS=$IFS
IFS=,
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read Appart RaspIP RaspUser RaspUserPassword aSystemUser aSystemPassword WiFiSSID WiFiPassword RouterPassword RouterVPNAddress RouterVPNPassword TunnelPort; do
if [[ $Appart == $apartm ]]
then
echo "Appartment : $Appart"
echo "WiFi SSID : $WiFiSSID"
echo "WiFi Password : $WiFiPassword"
sudo sed -i '9,10d' /etc/hostapd/hostapd.conf
sudo sed -i '$awpa_passphrase='$WiFiPassword /etc/hostapd/hostapd.conf
sudo sed -i '$assid='$WiFiSSID /etc/hostapd/hostapd.conf
sudo echo "pi:"$RaspUserPassword | sudo chpasswd
fi
done < $INPUT
IFS=$OLDIFS

sudo rm -R /home/pi/a.system/base/jbase/*
sudo cp -r $base_apartment/* /home/pi/a.system/base/
#ls | grep -v 'jbase \|WiFi_configurator.sh \|.git' | sudo xargs rm -rfv
#find . -type f -not -name "WiFi_configurator.sh" -not -name "Passwords.csv" -print0 | find . -type d -not -name "jbase" | sudo xargs rm -rfv
shopt -s extglob
sudo rm -rf ./!(jbase|.git|WiFi_configurator.sh)

cat /etc/hostapd/hostapd.conf